#!/usr/bin/python3

import sys
import qrcode
import json
from PIL import Image
import urllib.parse
import fasteners

def make_qrs(site_dir, logo_file):
    json_file = f'{site_dir}/data/wishlist.json'
    with fasteners.InterProcessLock(f'/tmp/{json_file.replace("/", "-")}.lock'):
        with open(json_file) as f:
            wishlist = json.load(f)
    for wish in wishlist[0]:
        address = wish["address"]
        #Monerujo have added a title to the json array, lets use that!
        title = wish["title"]
        print(f"{address}: {title}")

        qr = qrcode.QRCode(
            version=None,
            error_correction=qrcode.constants.ERROR_CORRECT_M,
            box_size=7,
            border=4
        )
        qr.add_data(f"monero:{address}?tx_description={urllib.parse.quote(title)}")
        qr.make(fit=True)

        img = qr.make_image(fill_color="black", back_color="white").convert("RGBA")
        # add logo
        logo = Image.open(logo_file).convert("RGBA")
        # assuming logo is square and smaller than the qr code
        offset = int((img.size[0]-logo.size[0])/2)
        img.paste(logo, (offset, offset), logo)

        img.save(f"{site_dir}/qr/{address}.png")

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print(f'Usage: {sys.argv[0]} <site directory> <logo.png>')
        sys.exit(4)

    make_qrs(sys.argv[1], sys.argv[2])
# example
#    make_qrs("/var/www/html", "gunther_head.png")
