let modified = "0"

async function getWishlist() {
    let url = 'data/wishlist.json?uid=' + Math.floor(Math.random() * 100000);
    try {
        let res = await fetch(url);
        return await res.json();
    } catch (error) {
        return null;
    }
}

async function renderWishlist() {
    let wishlist = await getWishlist();
    if (wishlist === null) return;
    modified_live = wishlist[1]["modified"]
    if (modified != modified_live){
        modified = modified_live
        let html = '';
        let id = 0
        wishlist[0].forEach(wish => {
            let imgcheck = document.getElementById(`img-${wish.address}`);
            let imgchecked = (imgcheck && imgcheck.checked)?" checked":"";

            let qrcheck = document.getElementById(`qr-${wish.address}`);
            let qrchecked = (qrcheck && qrcheck.checked)?" checked":"";

            let total = Math.trunc(wish.total.toFixed(3)*100)/100
            let isFunded = (wish.percent>=100)
	        let title = wish.url?`<a href="${wish.url}" target="_blank">${wish.title}</a>`:`${wish.title}`

	        let image = wish.image?
`<div class="donate-qr">
<input id="img-${wish.address}" type="checkbox" name="tabs" class="accordion image"${qrchecked}>
<label for="img-${wish.address}" class="accordion button">Toggle Nifty Gunther</label>
<div class="qr-content">
<p class="qr"><img src="${wish.image}"></a></p>
</div>
</div>`:''
 	        let qr =
`<div class="donate-qr">
<input id="qr-${wish.address}" type="checkbox" name="tabs" class="accordion qrcode"${qrchecked}>
<label for="qr-${wish.address}" class="accordion button">Toggle QR Code</label>
<div class="qr-content">
<p class="qr"><a href="monero:${wish.address}?tx_description=${encodeURIComponent(wish.title)}"><img src="qr/${wish.address}.png"></a></p>
</div>
</div>`
            let funded = wish.new?' <span class="funded">&nbsp;NEW&nbsp;</span>':(isFunded?' <span class="funded">&nbsp;FUNDED&nbsp;</span>':'')
            let progress = wish.progress?` <span class="progress">&nbsp;${wish.progress}&nbsp;</span>`:''
            let address = !isFunded?`<p class="subaddress point" id="${id}" onclick=CopyToClipboard('${wish.address}')>${wish.address}</p>`:`<p class="subaddress">${wish.address}</p>`
            let htmlSegment =
`<div class ="wish">
 <li>${title}${funded}${progress}
 ${wish.description?`<p class="subtext">${wish.description}</p>`:""}
 <p class="fundgoal">Raised ${total} of ${wish.goal} XMR &nbsp; <progress max="100" value="${wish.percent}">${wish.percent}%</progress> &nbsp; Contributors: ${wish.contributors}</p>
 ${address}
 ${image}
 ${!isFunded?qr:''}
 </li>
 </div>`

            html += htmlSegment;
            id += 1;
        });

        let container = document.getElementById('fr-container');
        container.innerHTML = html;
    }

}

function CopyToClipboard(text) {
    navigator.clipboard.writeText(text)
    .then(() => { alert(`Address Copied!`) })
    .catch((error) => { alert(`Copy failed! ${error}`) })
}

renderWishlist()
// update every minute for ever
setInterval(renderWishlist, 60000)
