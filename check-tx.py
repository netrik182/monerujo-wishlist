#!/usr/bin/python3

import sys
import json
from monerorpc.authproxy import AuthServiceProxy, JSONRPCException

json_file = "/var/www/html/data/wishlist.json"
rpc_url = "http://127.0.0.1:6666/json_rpc"

def main(address, txid, txkey):
    try:
        rpc_connection = AuthServiceProxy(rpc_url)
        info = rpc_connection.check_tx_key({"txid": txid, "tx_key": txkey, "address": address})
        xmr = info["received"]/1000000000000
        if xmr > 0:
            print(f'{info["confirmations"]} confirmations ' + ('POOL' if info["in_pool"] else 'BLOCK') + f' {xmr} XMR')
            with open(json_file) as f:
                wishlist = json.load(f)
            for i in range(len(wishlist[0])):
                if wishlist[0][i]["address"] == address:
                    print(wishlist[0][i]["title"])
                    break
        else:
            print('NO DICE')
    except Exception as e:
        print(e)
        raise e

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print("Usage: check-tx <adress> <txid> <txkey>")
    else:
        main(sys.argv[1], sys.argv[2], sys.argv[3])
