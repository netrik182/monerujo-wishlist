# Monerujo Wishlist

A Monero wishlist based on [xmr-wishlist-aaS](https://github.com/plowsof/xmr-wishlist-aaS). Special thanks to [u/twisted_turtles](https://libredd.it/u/twisted_turtles) and [u/KnowledgeMurky9635](https://libredd.it/u/KnowledgeMurky9635) for making that happen!

## About

**Monerujo Wishlist** is a dynamic showcase of projects which can be funded with XMR. Funding is done by associating Monero Subaddresses to the individual projects. Funding status is updated in real-time.

Currently there is no auto-update, so the page needs to be updated in the browser to show latest updates.

## How does it work?

The Python Script `wishlist-tx.py` is invoked by `monero-wallet-rpc` for each new transaction in the mempool for the corresponding wallet. This script updates the project data which is kept as a JSON file `data/wishlist.json` on the web server.

This data file is read by the JavaScript `js/app.js` and the details are displayed in all the right places on the page.

## Setup

1. Running `monero-wallet-rpc` connected to the View-Only Wallet to be monitored. See the `config` directory for sample configurations to get this up and running.
2. The `wishlist-tx.py` script deployed and it's path entered in the `tx-notify` parameter of `monero-wallet-rpc`.
3. Deployed `site` on a web server. It does not need to be in the root of the server.
4. Edit the `data/wishlist.json` - use the example provided. New entries can be created with `scripts/add_wish.py` - note that the optional fields `url` and `description` (and possibly others in the future) are not set by this script and need to be entered manually in the file if required.
5. Run `scripts/make_qrs.py` to generate all QR Codes - it embeds the title as `tx_description` in the URI so you should rerun it if you change the `title` or `address` of a project.

If your project funding is already ongoing, just add it as described above & adjust the `contributors`, `total` and `percent` fields in `data/wishlist.json` based on data you retreive from your wallet.

## Upgrading

1. Overwrite the deployed `site` (it leaves the `data/wishlist.json` intact)
2. Overwrite the deployed `wishlist-tx.py`

## Example Sites

You can find this in action at [Monerujo Funding](https://funding.monerujo.app/)

The [XMR Community Art Fund](https://moneroart.neocities.org/) uses the original [xmr-wishlist-aaS](https://github.com/plowsof/xmr-wishlist-aaS) by [\@plowsof](https://github.com/plowsof/)

## The Future

Future development will have sections for fully funded, ongoing &  deployed projects.
